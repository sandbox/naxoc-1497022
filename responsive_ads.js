(function ($) {

Drupal.behaviors.responsive_ads = {
  attach: function() {

    $.each($('div[class^="responsive-ads"]'), function() {

      if (Drupal.responsive_ads.shouldDisplay($(this).attr('data-minwidth'), $(this).attr('data-maxwidth'))) {
        if ($(this).attr('data-debug') == 1) {
          $(this).html('<div class="responsive-ads-debug" style="border: 2px black solid; padding: 5px;"><br />Ad:<br />' + $(this).attr('data-name') + '<br />Min width: ' + $(this).attr('data-minwidth') + '<br />Max width: ' + $(this).attr('data-maxwidth') + '</div>');
        }
        else {
          // Get the output we want to display for the ad.
        }
      }
    });
  }
}

Drupal.responsive_ads = Drupal.responsive_ads || {};

Drupal.responsive_ads.shouldDisplay = function(min, max) {
  var window_width = $(window).width();
  return (window_width > min && window_width < max);
};

})(jQuery);

